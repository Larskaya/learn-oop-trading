# coding: utf-8

class Buyer:
	def __init__(self, money, name):
		self.name = name
		self.money = money
		self.purchased_products = []
		self.service = []

	def buy(self, product, market):
		if self.money >= product.get_price() and market.is_on_shelves(product):
			self.purchased_products.append(product)
			self.money -= product.get_price()
			market.sell(product)

	

