# coding: utf-8

class Good:
	def __init__(self, name, price):
		self.name = name
		self.price = price

	def get_price(self):
		return self.price 

	def get_type(self):
		return self.type

	def __repr__(self):
		return '{Good obj: ' + self.name + '/' + str(self.price) + '}'