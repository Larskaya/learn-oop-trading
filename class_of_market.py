# coding: utf-8

from datetime import datetime, date
from class_of_product import Product
from class_of_service import Service


class Market:

	def __init__(self):
		self.money = 0
		self.eatable_shelf = []
		self.uneatable_shelf = []
		self.unsorted = []
		self.list_of_sold_products = []
		self.list_of_delete_products = []

	def get(self, product):
		self.unsorted.append(product)
		print('product is received')

	def get_list_of_products(self, products):
		for product in products:
			self.get(product)
			
	def sort_by_shelf(self):
		for product in self.unsorted:
			self.sort_one_by_shelf(product)
		self.unsorted = []
			
	def sort_one_by_shelf(self, product):
		if product.get_type() == 0:
			self.eatable_shelf.append(product)
		else:
			self.uneatable_shelf.append(product)

	def for_sort_by_price(self, product):
		return product.get_price()

	def sort_eatable_shelf(self):
		self.eatable_shelf.sort(key = self.for_sort_by_price)
		return self.eatable_shelf

	def sort_uneatable_shelf(self):
		self.uneatable_shelf.sort(key = self.for_sort_by_price)
		return self.uneatable_shelf

	def sort_all_shelfes(self):
		self.sort_eatable_shelf()
		self.sort_uneatable_shelf()

	def remove_products_from_the_shelf(self, product):
		if product in self.uneatable_shelf:
			self.uneatable_shelf.remove(product)
		elif product in self.eatable_shelf:
			self.eatable_shelf.remove(product)
		self.list_of_delete_products.append(product)

	def sell(self, product):
		if self.is_on_shelves(product):
			self.money += product.get_price()
			self.remove_products_from_the_shelf(product)
			self.list_of_sold_products.append(product)
		else: 
			print('product not on shelves')

	def is_on_shelves(self, product):
		return product in self.eatable_shelf or product in self.uneatable_shelf

	def delete(self, product):
		self.remove_products_from_the_shelf(product)
		self.list_of_delete_products.append(product)

	def check_product_date(self, product):
		today = date.today()
		if today >= product.find_out_the_date():
			self.remove_products_from_the_shelf(product)

	def get_all_price(self):

		print('new__', self.unsorted)
		print('polka e__', self.eatable_shelf)
		print('polka u__', self.uneatable_shelf)

		all_price = 0
		for product in self.eatable_shelf:
			all_price += product.get_price()
		for product in self.uneatable_shelf:
			all_price += product.get_price()
		return all_price

	def get_product_stage(self, product):
		if product in self.unsorted:
			stage = 'new'
		elif product in self.eatable_shelf:
			stage = 'sort on eatable shelf'
		elif product in self.uneatable_shelf:
			stage = 'sort on uneatable shelf'
		elif product in self.list_of_sold_products:
			stage = 'sold'
		elif product in self.list_of_delete_products:
			stage = 'delete'
		return stage

	def find_all_products_of_given_type(self, type_of_product):
		temporary_list = [] # временный список
		for product in Product.all_products:
			if product.get_specific_type() == type_of_product:
				temporary_list.append(product)
		return temporary_list

	def order_service(self, name_of_service, buyer):
		if name_of_service in Service.services_types:
			print('WORK')
			i = Service.services_types.index(name_of_service)
			if i == 0 and buyer.money >= Service.price_of_delivery:
				buyer.money -= Service.price_of_delivery
				self.money += Service.price_of_delivery
			elif i == 1 and buyer.money >= Service.price_of_notification:
				buyer.money -= Service.price_of_notification
				self.money += Service.price_of_notification
			s = Service(name_of_service, 0)
			buyer.service.append(s)
				
	# проверить наличие такого типа услуг
	# проверить наличие достаточного колва денежек у баера
	# создать экземпляр услуги, отнять денюшки и баера, добавить денюшки магазинику

	

	







