# coding: utf-8

from class_of_good import Good
print(Good, 'GGGOOODDD')

class Product(Good):
	types = [ 'eatable', 'uneatable' ]
	all_products = []

	@classmethod
	def search_product(self, vendor_code):
		for product in self.all_products:
			if vendor_code == product.vendor_code:
				return product

	def __init__(self, name, price, type_, specific_type, date, vendor_code):
		self.name = name
		self.price = price
		self.type = type_

		self.specific_type = specific_type
		self.date = date 
		self.vendor_code = vendor_code
		
		Product.all_products.append(self)

	def find_out_the_date(self):
		return self.date

	def get_specific_type(self):
		return self.specific_type

