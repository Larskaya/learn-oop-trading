# coding: utf-8

from class_of_good import Good

class Service(Good):
	services_types = ['delivery', 'notification']
	price_of_delivery = 500
	price_of_notification = 10

	def __init__(self, name, type_):
		self.name = name
		self.type = type_
		if self.type == 0:
			self.price = 500
		else:
			self.price = 10
