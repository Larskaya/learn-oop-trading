# coding: utf-8

from datetime import datetime, date
from class_of_market import Market
from class_of_product import Product
from class_of_buyer import Buyer
from class_of_good import Good

date1 = date(2019, 10, 12)
date2 = date(2018, 11, 2)
date3 = date(2018, 11, 1)
date4 = date(2018, 12, 2)

p1 = Product('хлеб', 100, 0, 'sweet', date1, 'A11111') # eatable
p2 = Product('вино', 250, 0, 'alcoholic', date2, 'A22222')
p3 = Product('масло', 250, 0, 'frozen', date3, 'A33333')
p4 = Product('ботинки', 1000, 1, 'non-flammable', date4, 'A44444') # uneatable
p5 = Product('бутылка', 300, 1, 'non-flammable', date4, 'A55555')

print(Product.all_products, 'all_products in Product')

m = Market()

m.get(p1)
m.get(p2)
m.get(p3)
m.get(p4)
m.get(p5)

print('product1 is', m.get_product_stage(p1), 'get')

m.sort_by_shelf()
print(m.get_all_price(), 'all price')
m.sort_all_shelfes()

print('product2 is', m.get_product_stage(p2), 'sell')

m.check_product_date(p1)

print(m.get_all_price(), 'all price')
print(Product.search_product('A22222'), 'product')

b = Buyer(500, 'asya')
#b.buy(p1, m)
#b.buy(p1, m)
#b.buy(p2, m)
print(b.purchased_products, 'buy')
print(m.get_all_price(), 'all price')

#g = Good('delivery', 500)

print(m.find_all_products_of_given_type('alcoholic'))

m.order_service('delivery', b)
print(b.service)
print(b.money, 'MONEY OF BUYER')





