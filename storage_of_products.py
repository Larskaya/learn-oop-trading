from datetime import datetime, date
from class_of_storage import Storage
from class_of_product import Product
from class_of_buyer import Buyer

date1 = date(2019, 10, 12)
date2 = date(2018, 11, 2)
date3 = date(2018, 11, 1)
date4 = date(2018, 12, 2)

p1 = Product('хлеб', 100, 0, date1, 'A11111')
p2 = Product('вода', 250, 0, date2, 'A22222')
p3 = Product('масло', 250, 0, date3, 'A33333')
p4 = Product('ботинки', 1000, 0, date4, 'A44444')
p5 = Product('бутылка', 300, 1, date4, 'A55555')

print(Product.all_products, 'all_products in Product')

s = Storage()

s.get(p1)
s.get(p2)
s.get(p3)
s.get(p4)
s.get(p5)

print('product1 is', s.get_product_stage(p1), 'get')

s.sort_by_shelf()
print(s.get_all_price(), 'all price')
s.sort_all_shelfes()

#s.delete(p2)

print('product2 is', s.get_product_stage(p2), 'sell')

s.check_product_date(p1)

print(s.get_all_price(), 'all price')
print(Product.search_product('A22222'), 'product')

b = Buyer(100, 'asya')
b.buy(p1, s)
b.buy(p1, s)
b.buy(p2, s)
print(b.purchased_products, 'buy')
print(s.get_all_price(), 'all price')