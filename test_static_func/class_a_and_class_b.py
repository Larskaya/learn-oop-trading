class A:
	@classmethod
	def a(self):
		return 'a'

class B(A):
	def __init__(self):
		self.b = 0

	print(A.a())